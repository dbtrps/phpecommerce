<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Cart";
	}

	function get_body_content(){
		require "../controllers/connection.php";
?>	
	<h1 class="text-center py-5">Cart Page</h1>
	<hr>
	<div class="col-lg10 offset-lg-1">
		<table class="table table-striped table-bordered">
			<thead>
				<tr class="text-center">
					<th>Item</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$total = 0;

					if(isset($_SESSION['cart'])){
						foreach($_SESSION['cart'] as $itemId => $quantity){
							$item_query = "SELECT * FROM items WHERE id = $itemId";
							$indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
							$subtotal = $indiv_item['price'] * $quantity;
							$total += $subtotal;
						?>
							<tr>
								<td><?php echo $indiv_item['name'] ?></td>
								<td><?php echo $indiv_item['price'] ?></td>
								<td>
									<span class="spanQ"><?php echo $quantity ?></span>
									<form action="../controllers/add-to-cart-process.php" method="POST" class="d-none">
										<input type="hidden" name="id" value="<?php echo $itemId ?>">
										<input type="hidden" name="fromCartPage" value="fromCartPage">
										<input type="number" class="form-control" name="cart" value="<?php echo $quantity ?>" data-id="<?php echo $itemId ?>">
									</form>
								</td>
								<td><?php echo number_format($subtotal, 2) ?></td>
								<td><a href="../controllers/remove-from-cart-process.php?id=<?php echo $itemId?>" class="btn btn-warning">Remove</a></td>
							</tr>
						<?php
						}
					}
				 ?>
				 <tr>
				 	<td></td>
				 	<td></td>
				 	<td id="totalPayment" class="font-weight-bold">Total:</td>
				 	<td><?php echo number_format($total, 2) ?></td>
				 	<td><a href="../controllers/empty-cart-process.php" class="btn btn-danger">Empty Cart</a></td>
				 </tr>
				 <tr>
				 	<td></td>
				 	<td></td>
				 	<td>
				 		<form action="../controllers/checkout-process.php" method="POST">
				 		<input type="hidden" name="totalPayment" value="<?php echo $total ?>">
				 		<input type="hidden" name="cod" value="total">
				 		<button type="submit" class="btn btn-info">Pay via cash</button>
				 	</form>
				 	</td>
				 	<td><div id="paypal-button-container"></div></td>
				 	<td></td>
				 </tr>
			</tbody>
		</table>
	</div>
<script type="text/javascript" src="../assets/scripts/update-cart.js"></script>

<!-- paypal -->
<script src="https://www.paypal.com/sdk/js?client-id=AVfbK9B3zIJr4EEEdW8N3gNhNTOZ4rf8oYjsep3IrBxsDv76l073mFmqNbPMvg8bsNuxJfZ787FCN8Qi"> </script>

  <script>
  	let totalPayment = document.getElementById('totalPayment').textContent.split(',').join("");
    paypal.Buttons({
    	createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: totalPayment
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
      	let data = new FormData;
      	data.append('totalPayment', totalPayment);
      	data.append('fromPaypal', 'fromPaypal');

      	fetch("../controllers/checkout-process.php", {
      		method:"POST",
      		body:data
      	}).then(res=>res.text()).then(res=>{
      		console.log(res);
      		alert('Transaction completed by ' + details.payer.name.given_name);	
      	})
        // This function shows a transaction success message to your buyer.
        
      });
    }
    }).render('#paypal-button-container');
    // This function displays Smart Payment Buttons on your web page.
  </script>
<?php

	}
 ?>