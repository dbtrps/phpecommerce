<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Address";
	}

	function get_body_content(){
		require "../controllers/connection.php";
		$user_id = $_SESSION['user']['id'];
 ?>
 <h1 class="text-center py-5">Add Address</h1>
 <div class="col-lg-8 offset-lg-2">
 	<form action="../controllers/add-address-process.php" method="POST">
		<div class="form-group">
			<label for="address1">Address 1:</label>
			<input type="text" name="address1" class="form-control">
		</div>
		<div class="form-group">
			<label for="address2">Address 2:</label>
			<input type="text" name="address2" class="form-control">
		</div>
		<div class="form-group">
			<label for="city">City:</label>
			<input type="text" name="city" class="form-control">
		</div>
		<div class="form-group">
			<label for="zip_code">Zip Code:</label>
			<input type="text" name="zip_code" class="form-control">
		</div>
			<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
			<button class="btn btn-secondary" type="submit">Add Address</button>
	</form>
 </div>	
<?php
	}
 ?>