<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Profile";
	}

	function get_body_content(){
		require "../controllers/connection.php";
?>
	<h1 class="text-center py-5">Profile page</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<h3>User profile</h3>
				<label>First Name</label>
				<h6><?php echo $_SESSION['user']['firstName'] ?></h6>
				<label>Last Name</label>
				<h6><?php echo $_SESSION['user']['lastName'] ?></h6>
				<label>Email</label>
				<h6><?php echo $_SESSION['user']['email'] ?></h6>
			</div>
			<div class="col-lg-4">
				<h3>Contacts</h3>
				<label>Email</label>
				<h6><?php echo $_SESSION['user']['email'] ?></h6>
				<label>Contact number</label>
				<h6><?php echo $_SESSION['user']['contactNo'] ?></h6>
				<a href="">Edit contact</a>
			</div>
			<div class="col-lg-4">
				<h3>Addresses</h3>
				<ul>
					<?php 	
						$user_id = $_SESSION['user']['id'];
						$address_query = "SELECT * FROM addresses WHERE user_id = $user_id";
						$addresses = mysqli_query($conn, $address_query);
						foreach ($addresses as $indiv_address) {
						?>
						<li><?php echo $indiv_address['address1'] . ", " . $indiv_address['address2']. "<br>" . $indiv_address['city'] . " " . $indiv_address['zip_code'] ?></li>
						<?php
						}
					 ?>
				</ul>
				<a href="add-address.php">Add a new address</a>
			</div>
		</div>
	</div>
<?php
	}
 ?>