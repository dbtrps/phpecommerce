<?php 
	require "../partials/template.php";

	function get_title(){
		echo "login";
	}

	function get_body_content(){

	}
	?>
	<h1 class="text-center py-5">Login</h1>
	<div class="col-lg-8 offset-lg-2">
		<form action="" method="POST">
			<div class="form-group">
				<label for="email">Email: </label>
				<input type="email" class="form-control" name="email" id="email">
				<span class="validation"></span>
			</div>
			<div class="form-group">
				<label for="password"></label>
				<input type="password" class="form-control" name="password" id="password">
				<span class="validation"></span>
			</div>
			<button type="button" class="btn btn-info" id="loginUser">Login</button>
			<p>Not yet registered? <a href="login.php">Register</a></p>
		</form>
	</div>
	<script type="text/javascript" src="../assets/scripts/login.js"></script>
	<?php
 ?>