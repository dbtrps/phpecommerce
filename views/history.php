<?php 
	require "../partials/template.php";

	function get_title(){
		"History";
	}

	function get_body_content(){
	?>
	<div class="col-lg-6 offset-lg-6">
		<table class="table">
			<thead>
				<tr>
					<td>Order Id</td>
					<td>Items</td>
					<td>Price</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					require '../controllers/connection.php';
					$email = $_SESSION['user']['email'];
					$orders = mysqli_query($conn, "SELECT * FROM orders WHERE user_id = (SELECT id FROM users WHERE email='$email')");
					foreach($orders as $order){
						$orderId = $order['id'];
					?>
					<tr>
						<td><?php echo $orderId ?></td>
						<td>
							<?php $items = mysqli_query($conn, "SELECT * FROM items WHERE id IN (SELECT item_id FROM item_order WHERE order_id=$orderId)");
								
								foreach($items as $item){
									echo $item['name'] . "<br>";
								}
							?>
								
						</td>
						<td><?php echo $order['total'] ?></td>
					</tr>
					<?php 
					}
				 ?>
			</tbody>
		</table>
	</div>
	<?php
	}
 ?>