<?php 
	$host = "localhost"; //the host that will use;
	$db_username = "root"; //username for the host;
	$db_password = ""; //the password the host will set, empty string;
	$db_name = "b55Ecommerce"; //database name;

	// creates the connection between databases;
	$conn = mysqli_connect($host, $db_username, $db_password, $db_name);

	// to check the connection;
	if(!$conn){
		diw("Connection failed: " . mysqli_error($conn));
	}
 ?>