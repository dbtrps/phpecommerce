<!DOCTYPE html>
<html>
<head>
	<!-- bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">

	<title><?php get_title(); ?></title>

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand" href="#">UAPLG</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor02">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
	      </li>
	     <?php 
	      	session_start();
	      	// var_dump($_SESSION);
	      	if(isset($_SESSION['user'])&& $_SESSION['user']['role_id']== 1){
	     ?>
	      	<li class="nav-item">
	        	<a class="nav-link" href="add-item.php">Add item</a>
	      	</li>
	      	<li>
	      		<a class="nav-link" href="transaction.php">Transaction</a>
	      	</li>
	     <?php
	      	}else{
	     ?>
	     	<li class="nav-item active">
	        <a class="nav-link" href="cart.php">Cart <span class="badge bg-warning" id="cartCount">
	        <?php 
	        	if(isset($_SESSION['cart'])){
	        		echo array_sum($_SESSION['cart']);
	        	}else{
	        		echo 0;
	        	}
	         ?>
	        </span></a>
	      	</li>
	      	<li>
	      		<a class="nav-link" href="history.php">History</a>
	      	</li>
	    <?php		
	      }
	      if(isset($_SESSION['user'])){
	     ?>
	     	<li class="nav-item">
	     		<a href="#" class="nav-link">Hello <?php echo $_SESSION['user']['firstName'] ?>!</a>
	     	</li>
	     	<li class="nav-item">
	     		<a href="../controllers/logout-process.php" class="nav-link">Logout</a>
	     	</li>
	     <?php
	      }else{
	     ?>
		     <li class="nav-item">
		     	<a href="login.php" class="nav-link">Login</a>
		     </li>
		     <li class="nav-item">
		     	<a href="register.php" class="nav-link">Register</a>
		     </li>
	     <?php
	      }
	    ?>
	      <li class="nav-item">
	        <a class="nav-link" href="catalog.php">Catalog</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="register.php">Register</a>
	      </li>
	    </ul>
	    <form class="form-inline my-2 my-lg-0">
	      <input class="form-control mr-sm-2" type="text" placeholder="Search">
	      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
	    </form>
	  </div>
	</nav>

<?php get_body_content(); ?>

	<footer class="page-footer bg-black font-small">
		<div class="text-center py-3 text-white">
			© 2020 Copyright 
		</div>
	</footer>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>